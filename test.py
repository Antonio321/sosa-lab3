import unittest
import math
from dodatak_A import OperationsManager

class TestOperationsManager(unittest.TestCase):
    def __init__(self, methodName: str = ...) -> None:
        self.om = OperationsManager(1, 10)
        super().__init__(methodName)

    def test_addition(self):
        self.assertEqual(self.om.a + self.om.b, self.om.perform_addition())

    def test_subtraction(self):
        self.assertEqual(self.om.a - self.om.b, self.om.perform_subtraction())
    
    def test_multiplication(self):
        self.assertEqual(self.om.a * self.om.b, self.om.perform_multiplication())

    def test_multiplication_b_zero(self):
        tmp, self.om.b = self.om.b, -1
        self.assertEqual(-1, self.om.perform_multiplication())
        self.om.b = tmp

    def test_division(self):
        self.assertEqual(self.om.a / self.om.b, self.om.perform_division())
    
    def test_division_b_zero(self):
        tmp, self.om.b = self.om.b, 0
        self.assertTrue(math.isnan(self.om.perform_division()))
        self.om.b = tmp


if __name__ == '__main__':
    unittest.main()