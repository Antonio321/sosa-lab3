import getpass
import math
import hashlib
class OperationsManager():
    def __init__(self, a: float, b: float) -> None:
        self.a = a
        self.b = b

    def perform_addition(self) -> float:
        """Adds a with b."""
        return self.a + self.b

    def perform_subtraction(self) -> float:
        """Subtracts b from a."""
        return self.a - self.b

    def perform_multiplication(self) -> float:
        """Multiplies a with b. If b is zero, returns -1."""
        return -1 if self.b == 0 else self.a * self.b
    
    def perform_division(self) -> float:
        """Divides a with b. If b is zero, returns NaN."""
        return math.nan if self.b == 0 else self.a / self.b
    

if __name__ == "__main__":
    user = input("Username: ")
    password = getpass.getpass("Password: ")
    if user != "root" or hashlib.sha3_256(bytes(password, 'utf-8')).digest() != b'>^]\x0f\xa0\xcf\x19\xd7]-\xeel\xf7\x03\xfcIJ8\xe4P\x9c\xf9OC\xdf\xe7\x17\xa8\xdc\x8e8\xcc':
        print("Wrong username or password!")
        exit(0)
    else:
        print("Login success!")
        a = float(input("A = "))
        b = float(input("B = "))
        ops_manager = OperationsManager(a, b)
        print(ops_manager.perform_division())
